<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>

<body>

	<?php
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
	?>
		<h3>strpos() must have returned non-false</h3>
		<p>You are using Internet Explorer</p>
	<?php
	} else {
	?>
		<h3>strpos() must have returned false</h3>
		<p>You are not using Internet Explorer</p>
	<?php
	}
	?>

</body>

</html>